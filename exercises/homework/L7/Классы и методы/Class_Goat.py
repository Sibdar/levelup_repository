# Класс Goat .
# Атрибуты: возраст, имя, номер загона, сколько молока дает в день.
# Методы:
#** издать звук,
#** напечатать имя и возраст,
#** получить номер загона,
#** получить сколько молока дает в день.


class Goat:
    def __init__(self,name,age,yard_no,milk_per_day):
        self.name = name
        self.age = age
        self.yard_no = yard_no
        self.milk_per_day = milk_per_day

    def get_goat_sound(self):
        print('Бе-беее-беееее...')

    def print_name_and_age(self):
        print(f'Имя: {self.name}; возраст: {self.age}')

    def get_yard_no(self):
        return self.yard_no

    def get_milk_per_day(self):
        return self.milk_per_day


def main():
    pass


if __name__ == "__main__":
    main()