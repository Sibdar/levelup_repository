# Класс Chicken .
# Атрибуты: имя, номер загона, сколько яиц в день.
# Методы:
#** издать звук,
#** напечатать имя,
#** получить номер загона,
#** получить сколько яиц дает в день.


class Chicken:
    def __init__(self,name,yard_no,eggs_per_day):
        self.name = name
        self.yard_no = yard_no
        self.eggs_per_day = eggs_per_day

    def get_chicken_sound(self):
        print('Ко-ко-ко-ко-ко!')

    def print_name(self):
        print(f'Имя курки: {self.name}')

    def get_yard_no(self):
        return self.yard_no

    def get_eggs_per_day(self):
        return self.eggs_per_day


def main():
    pass


if __name__ == "__main__":
    main()