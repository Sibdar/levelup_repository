# Класс Pupil .
# Атрибуты: средний балл, возраст, имя.
# Методы: получить средний балл, получить возраст, получить имя.

class Pupil:
    def __init__(self, avrg_score, pupil_age, pupil_name):
        self.avrg_score = avrg_score
        self.pupil_age = pupil_age
        self.pupil_name = pupil_name
    
    def get_avrg_score(self):
        return self.avrg_score
    
    def get_pupil_age(self):
        return self.pupil_age

    def get_pupil_name(self):
        return self.pupil_name

########################################################################################################
#Класс Worker .
# Атрибуты: имя, возраст, стаж работы, зарплата, должность.
# Методы:
#       напечатать строку “Имя: <имя> | Возраст: <возраст> | Должность: <должность> | Стаж: <стаж работы> | ЗП: <зарплата>”,
#       получить имя, получить возраст, получить зп, получить должность.

class Worker:
    def __init__(self, name, age, work_experience, salary, position):
        self.name = name
        self.age = age
        self.work_experience = work_experience
        self.salary = salary
        self.position = position

    def print_worker_info(self):
        print(f'Имя: {self.name} | Возраст: {self.age} | Должность: {self.position}'
              f' | Стаж: {self.work_experience} | Зарплата: {self.salary} RUB')

    def get_worker_name(self):
        return self.name

    def get_worker_age(self):
        return self.age

    def get_worker_salary(self):
        return self.salary

    def get_worker_position(self):
        return self.position

########################################################################################################
# Класс School .
# Атрибуты: список людей в школе (общий list для Pupil и Worker ), номер школы.
# Методы:
#        получить средний балл всех учеников школы V
#        получить среднюю зп работников школы V
#        получить сколько всего работников в школе V
#        получить сколько всего учеников в школе V
#        получить средний возраст работников школы
#        получить средний возраст учеников школы
#        получить средний стаж работников школы
#        получить все имена учеников (с повторами, если есть)
#        получить все должности работников (без повторов)
#        получить возраст самого старшего ученика
#        получить самую большую зп работника
#        получить имя работника с самым маленьким стажем работы
#        получить возраст ученика с самым низким средним баллом

class School:
    def __init__(self, school_number):
        self.people_in_school = []
        self.school_number = school_number

    # добавление людей в школу
    def append_person_to_school(self, *persons):
        self.people_in_school.extend(persons)

    # средний бал учеников в школе
    def get_avrg_score(self):
        pupils_common_score = sum([x.get_avrg_score() for x in self.people_in_school if type(x) == Pupil])
        return f'Средний балл учеников: {round(pupils_common_score/self.get_pupils_number(), 2)}'

    # средняя з/п работников в школе
    def get_avrg_salary(self):
        workers_common_salary = sum([x.get_worker_salary() for x in self.people_in_school if type(x) == Worker])
        return f'Средняя з/п работников: {workers_common_salary/self.get_workers_number()}'

    # кол-во учеников в школе
    def get_pupils_number(self):
        return len([x for x in self.people_in_school if type(x) == Pupil])

    # кол-во работников в школе
    def get_workers_number(self):
        return len([x for x in self.people_in_school if type(x) == Worker])

    # средний возраст работников школы
    def get_avg_worker_age(self):
        workers_common_age = sum([x.get_worker_age() for x in self.people_in_school if type(x) == Worker])
        return f'Средний возраст работников: {workers_common_age/self.get_workers_number()}'

    # средний возраст учеников школы
    def get_avg_pupils_age(self):
        pupils_common_age = sum([x.pupil_age for x in self.people_in_school if type(x) == Pupil])
        return f'Средний возраст учеников: {round(pupils_common_age/self.get_pupils_number(), 2)}'

    # средний стаж работников школы
    def get_avrg_exp(self):
        return f'Средний стаж работников: {sum([x.work_experience for x in self.people_in_school if type(x) == Worker])/self.get_workers_number()}'

    # все имена учеников (с повторами, если есть)
    def get_pupil_names(self):
        return f"Имена учеников в школе: {', '.join([x.get_pupil_name() for x in self.people_in_school if type(x) == Pupil])}"

    # все должности работников (без повторов)
    def get_worker_positions(self):
        return f"Должности работников: {', '.join({x.position for x in self.people_in_school if type(x) == Worker})}"

    # возраст самого старшего ученика
    def get_oldest_pupil(self):
        return f"Возраст старшего ученика: {max([x.get_pupil_age() for x in self.people_in_school if type(x) == Pupil])}"

    # самая большая з/п работника
    def get_highest_salary(self):
        return f'Самая большая з/п: {max([x.get_worker_salary() for x in self.people_in_school if type(x) == Worker])}'

    # имя работника с самым маленьким стажем
    def get_worker_name_with_least_exp(self):
        # наименьший стаж
        least_worker_exp = min([x.work_experience for x in self.people_in_school if type(x) == Worker])
        return f'Имя работника с наименьшим стажем: ' \
               f"{''.join([x.name for x in self.people_in_school if type(x) == Worker and x.work_experience == least_worker_exp])}"

    # возраст ученика с самым низким средним баллом
    def get_pupil_age_with_least_score(self):
        # наименьший средний балл
        least_pupil_score = min([x.get_avrg_score() for x in self.people_in_school if type(x) == Pupil])
        return f'Возраст ученика с самым низким средним балом: ' \
               f'{[x.pupil_age for x in self.people_in_school if type(x) == Pupil and x.get_avrg_score() == least_pupil_score][0]}'


def main():
    Masha = Pupil(4.5, 9, 'Masha')
    Valera = Worker('Valera', 27, 7, 70000, 'Director')
    School_No25 = School(25)
    School_No25.append_person_to_school(Masha, Pupil(5, 9, 'Ilia'), Pupil(5, 8, 'Natasha'), Pupil(4.8, 9, 'Lesha'),
                                        Pupil(3, 9, 'Alexander'), Pupil(3.3, 10, 'Vasiliy'), Pupil(4, 9, 'Alex'), Valera,
                                        Pupil(4.7, 8, 'Dasha'), Pupil(2, 9, 'Sergei'),
                                        Worker('Pavel', 45, 11, 63000, 'Teacher'), Pupil(4.2, 7, 'Lesha'),
                                        Worker('Grigoriy', 53, 15, 65000, 'Teacher'))

    # вывести средний балл учеников школы
    print(School_No25.get_avrg_score())

    # вывести среднюю з/п работников
    print(School_No25.get_avrg_salary())

    # вывести кол-во работников
    print(f'Кол-во работников: {School_No25.get_workers_number()}')

    print(f'Кол-во учеников: {School_No25.get_pupils_number()}')

    # вывести средний возраст работников
    print(School_No25.get_avg_worker_age())

    # вывести средний возраст учеников
    print(School_No25.get_avg_pupils_age())

    # вывести средний страж работников школы
    print(School_No25.get_avrg_exp())

    # вывести все имена учеников в школе
    print(School_No25.get_pupil_names())

    # вывести должности работников
    print(School_No25.get_worker_positions())

    # вывести возраст старшего ученика
    print(School_No25.get_oldest_pupil())

    # вывести самую большую зарплату среди работников
    print(School_No25.get_highest_salary())

    # вывести имя работника с самым маленьким стажем
    print(School_No25.get_worker_name_with_least_exp())

    # вывести возраст ученика с самым маленьким стажем
    print(School_No25.get_pupil_age_with_least_score())


if __name__ == '__main__':
    main()
