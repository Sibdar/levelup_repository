# Класс Dog . Атрибуты age , name .
# При создании экземпляра инициализировать атрибуты класса.
# Создать метод print_park , который печатает “Гав-гав-гав. Это мое имя name на моем собачьем языке!”.
# Создать метод get_age , который возвращает значение атрибута age.

class Dog:
    def __init__(self,age,name):
        self.age = age
        self.name = name

    def print_park(self):
        print(f'Гав-гав-гав. Это мое имя "{self.name}" на собачьем языке!')

    def get_age(self):
        return self.age

def main():
    Gav_Dog = Dog(4,'Курва')
    Gav_Dog.print_park()

if __name__ == "__main__":
    main()