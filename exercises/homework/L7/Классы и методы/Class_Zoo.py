# Создать класс Zoo .
# Атрибуты animal_count (количество животных), name и money (сколько денег на счету).
# При создании экземпляра инициализировать атрибуты класса.
# Создать метод get_animal_count , который возвращает количество животных animal_count в зоопарке.
# Создать метод print_name , который печатает строку “ name - это лучший зоопарк!”.
# Создать метод can_afford ,
#  который принимает аргумент needed_money и возвращает True
#  если зоопарк может заплатить столько денег (self.money >= needed_money), иначе False.

class Zoo:
    def __init__(self,animal_count,name,money):
        self.animal_count = animal_count
        self.name = name
        self.money = money

    def get_animal_count(self):
        return self.animal_count

    def print_name(self):
        print(f'{self.name} - это лучший зоопарк')

    def can_afford(self, needed_money):
        return self.money >= needed_money


def main():
    Zoo_Rus = Zoo(45,'Russian Zoo',520500)

    # проверка атрибутов
    print(f'{Zoo_Rus.animal_count} | {Zoo_Rus.name} | {Zoo_Rus.money}')

    # проверка метода get_animal_count()
    print(Zoo_Rus.get_animal_count())

    # проверка метода print_name()
    Zoo_Rus.print_name()

    # проверка метода can_afford()
    print(Zoo_Rus.can_afford(600000))

if __name__ == "__main__":
    main()
