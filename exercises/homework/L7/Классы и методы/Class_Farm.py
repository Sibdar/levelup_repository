# Класс Farm .
# Атрибуты: животные (list из произвольного количества Goat и Chicken),
#           наименование фермы, имя владельца фермы.
# Методы:
#** напечатать наименование и владельца фермы,
#** получить количество коз на ферме (*Подсказка isinstance или type == Goat),
#** получить количество куриц на ферме,
#** получить количество животных на ферме,
#** получить сколько молока можно получить в день,
#** получить сколько яиц можно получить в день.


from Class_Chicken import Chicken
from Class_Goat import Goat


class Farm:
    def __init__(self, farm_name, farm_owner):
        self.livestock = []
        self.farm_name = farm_name
        self.farm_owner = farm_owner

    def print_farm_name_and_owner(self):
        print(f'Имя фермы: {self.farm_name}, владелец: {self.farm_owner}')

    def get_goat_number(self):
        return f'Коз на ферме: {len([x for x in self.livestock if type(x) == Goat])}'

    def get_chicken_number(self):
        return f'Кур на ферме: {len([x for x in self.livestock if type(x) == Chicken])}'

    def get_livestock(self):
        return f'Животных на ферме: {len(self.livestock)}'

    # общее кол-во молока в день
    def get_general_milk_per_day(self):
        return f'Потенциальное кол-во молока в день: {sum([x.get_milk_per_day() for x in self.livestock if type(x) == Goat])} л.'

    # общее кол-во яиц в день
    def get_general_eggs_per_day(self):
        return f'Потенциальное кол-во яиц в день: {sum([x.get_eggs_per_day() for x in self.livestock if type(x) == Chicken])} шт.'

    # добавление животных на ферму
    def extend(self, *animals):
        self.livestock.extend(animals)


def main():
    My_Farm = Farm("My Farm",'Ilia')
    Chicken_1 = Chicken('Бурка',2,4)
    My_Farm.extend(Chicken_1, Goat('Алёша',3,2,10),Chicken('Курка',2,3),Chicken('Македонка',2,5),Goat('Милиса',3,2,9))
    print(My_Farm.get_livestock())
    print(My_Farm.get_goat_number())
    print(My_Farm.get_chicken_number())
    print(My_Farm.get_general_milk_per_day())
    print(My_Farm.get_general_eggs_per_day())


if __name__ == "__main__":
    main()


