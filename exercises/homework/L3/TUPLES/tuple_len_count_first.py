#Функция tuple_len_count_first .
# Принимает 2 аргумента: кортеж “ассорти” my_tuple (который создалпользователь) и число num.
# Возвращает кортеж состоящий из длины кортежа, количества чисел num в кортеже my tuple и первого элемента кортежа.
# Пример: my_tuple=(‘55’, ‘aa’, 66) num = 66, результат (3, 1, ‘55’)

def tuple_len_count_first(tuple,num):
    return len(tuple),tuple.count(num),tuple[0]


def main():
    my_tuple = (34, 'sdfsdf', 'sdfsf', 3435, 'df', 'школа', 'камень', 345, 9, 9)
    print(tuple_len_count_first(my_tuple,9))


if __name__ == '__main__':
    main()