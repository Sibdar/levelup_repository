# Функция tuple_len_first .
# Принимает 1 аргумент: кортеж “ассорти” my_tuple (который создал пользователь).
# Возвращает кортеж состоящий из длины кортежа и первого элемента кортежа.
# Пример: (‘55’, ‘aa’, 66), результат (3, ‘55’).

def tuple_len_first(tuple):
    return len(tuple), tuple[0]



def main():
    my_tuple = ('sdf', 3, 'sdf', 34, 56, 78, 'sdfsdf', 'крочкаэ')
    print(tuple_len_first(my_tuple))


if __name__ == '__main__':
    main()