# Функция get_translation_by_word .
# Принимает 2 аргумента: ru-eng словарь содержащий ru_word: [eng_1, eng_2 ...] и слово для поиска в словаре (ru).
# Возвращает все варианты переводов, если такое слово есть в словаре, если нет, то ‘Can’t find Russian word: {word}’.

def get_translation_by_word(dict, word):
    return dict[word]


def main():
    while True:
        ru_eng_dict = {
                        'яблоко': 'apple',
                        'ананас': 'pineapple',
                        'виноград': 'grape',
                        'персик': 'peach',
                        'абрикос': 'apricot'
                            }
        word_to_search = input('Введите слово и я переведу его на английский: ')
        print('Вот ваш перевод: ' + str(get_translation_by_word(ru_eng_dict,word_to_search)))


if __name__ == "__main__":
    main()