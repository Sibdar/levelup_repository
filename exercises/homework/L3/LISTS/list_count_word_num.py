# Функция list_count_word_num .
# Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
# Возвращает количество чисел и количество слов(букв) в списке my_list.

def list_count_word_num(lst):
    num_count = 0
    word_count = 0
    for i in lst:
        if i.isdigit():
            num_count += 1
        else:
            word_count += 1
    return f'\nКоличество слов в списке: {word_count}\nКоличество цифр в списке: {num_count}'

def main():
    my_input = input('Введите перечень данных через пробел: ')
    my_list = my_input.split(' ')
    print(list_count_word_num(my_list))

if __name__ == "__main__":
    main()