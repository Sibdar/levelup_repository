
#  int(i) for i in lst

# просто стркоа
my_string = 'я жук 5 ушел куда глаза ушел 54 жук мой мой дом 54 5 я я я'

# трансформируем строку в лист
lst = my_string.split(' ')

# итерация с целью поменять тип данных цифр со string на int
for i in lst:
    # если цифра
    if i.isdigit():
        # находим индекс цифры в листе
        i_index = lst.index(i)
        # меняем тип данных по найденному индексу со string на int
        lst[i_index] = int(i)
print(lst)
