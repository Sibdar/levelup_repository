# Функция list_count_word.
# Принимает 2 аргумента: список слов my_list (который создал пользователь) и строку word.
# Возвращает количество word в списке my_list.

def list_count_num(lst,word):
    return lst.count(word)

def main():
    words_stre = input("Введите перечень слов через пробел: ")
    word_stre = input("Введите строку для поиска в листе: ")
    words_lst = words_stre.split(' ')
    print(words_lst)
    print(list_count_num(words_lst,word_stre))


if __name__ == "__main__":
    main()