# Функция list_if_found .
# Принимает 3 аргумента: список “ассорти” my_list (который создал пользователь), число num и строку word.
# Возвращает:
##   “I found {word}”, если найдена только строка word;
##   “I found {num}”, если найдено только число num;
##   “I found {word} and {num}!!!”, если найдены оба;
##   “Sorry, I can’t find anything...”, если не найдено ни слово ни число. (* тип num и word важен).

def list_if_found(lst, num, word):
    if (word,num) in lst:
    #if {num, word}.issubset(set(lst)):
        return f'I found "{word}" and "{num}"!!!'
    elif word in lst:
        return f'I found "{word}"'
    elif num in lst:
        return f'I found "{num}"'
    else:
        return "Sorry, I can't find anything..."


def main():
    my_list = ['я', 'жук', 5, 'ушел', 'куда', 'глаза', 'ушел', 54, 'жук', 'мой', 'мой', 'дом', 54, 5, 'я', 'я', 'я']
    num_to_find = 54
    word_to_find = 'глаза'
    print(list_if_found(my_list, num_to_find, word_to_find))


if __name__ == '__main__':
    main()
