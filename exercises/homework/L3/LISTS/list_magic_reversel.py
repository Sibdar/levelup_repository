# Функция list_magic_reversel . Принимает 1 аргумент: список “ассорти” my_list (который создал пользователь).
# Создает новый список new_list (копия my_list), порядок которого обратный my_list.
# Возвращает список, который состоит из [второго элемента new_list] + [предпоследнего элемента new_list] + [весь new_list].
# Пример: входной список [1, ‘aa’, 99], new_list [99, ‘aa’, 1], результат [1, 1, 99, ‘aa’, 1].
#   (* Если индекс выходит за пределы списка, то последний. Если список пустой - вернуть пустой список.
#   Нумерация элементов начинается с 0 .)
import operator


def list_magic_reversel(lst):
    new_lst = lst.copy()
    new_lst.reverse()
    # если не введены никакие данные в input
    if '' in lst and len(lst) == 1:
        return 'Данных не введено: ' + str([])
    # индекс не выходит за пределы, все ок
    elif len(new_lst) >= 2:
        return [new_lst[1],new_lst[-2]] + new_lst
    # если индекс выходит за пределы списка (len < 2)
    else:
        return f'Индекс выходит за пределы списка, поэтому вывожу: "{new_lst[-1]}"'


def main():
    while True:
        string_for_list = input('Введите данные через пробел: ')
        my_list = string_for_list.split(' ')
        # типизируем цифры в листе в 'int'
        for i in my_list:
            if i.isdigit():
                # получаем индекс цифры в листе
                i_ind = my_list.index(i)
                # изменяем тип цифры по индексу
                my_list[i_ind] = int(i)
        print(list_magic_reversel(my_list))


if __name__ == "__main__":
    main()