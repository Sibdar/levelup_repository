#Функция list_count_num .
#Принимает 2 аргумента: список числами my_list (который создал пользователь) и число num.
#Возвращает количество num в списке my_list.

def list_count_num(lst,num):
    return lst.count(num)

def main():
    my_list = [1, 5, 445, 5, 5, 56, 4, 5, 45, 4, 55, 5, 4, 55, 5, 55, 56, 4565, 4456, 564, 456, 456, 5, 5, 5, 5, 55, 454, 88, 6, 55, 7, 8, 9]
    num = 445
    print(list_count_num(my_list,num))


if __name__ == "__main__":
    main()