# Функция set_print_info_for_2 .
# Принимает 2 аргумента: множества “ассорти” my_set_left и my_set_right (которые создал пользователь).
# Выводит информацию о:
#   > равенстве множеств
#   > имеют ли они общие элементы
#   > является ли my_set_left подмножеством my_set_right и наоборот.

def set_print_info_for_2(set1, set2):
    return f'set1 равен set2: ' + str(set1 == set2) + ', \n' \
           +'set1 и set2 не имеют общих элементов: ' + str(set1.isdisjoint(set2)) + ', \n' \
           + 'set1 подмножество set2: ' + str(set1.issubset(set2)) + ', \n' \
           + 'set2 подмножество set1: ' + str(set2.issubset(set1))


def main():
    my_set_left = {1, 3, 5, 6, 7, 8}
    my_set_right = {6, 7, 8}
    print(set_print_info_for_2(my_set_left, my_set_right))


if __name__ == '__main__':
    main()
