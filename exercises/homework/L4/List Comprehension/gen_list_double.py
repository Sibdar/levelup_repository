# Функция gen_list_double .
# Принимает число n.
# Возвращает список длиной n, состоящий из удвоенных значений от 0 до n.
# Пример: n=3, результат [0, 2, 4].

def gen_list_double(n):
    return [n*2 for n in range(n)]


def main():
    num = int(input('Введите число и я кое-что сделаю: '))
    print(gen_list_double(num))


if __name__ == "__main__":
    main()