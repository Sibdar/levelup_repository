# Функция gen_list_pow_odd .
# Принимает число n. Возвращает список длиной n, состоящий из квадратов нечетных чисел в диапазоне от 0 до n.
# Пример: n = 7, четные числа [1, 3, 5], результат [1, 9, 25].


def gen_list_pow_odd(n):
    return [n ** 2 for n in range(n) if n % 2 != 0]


def main():
    n = int(input('Введите число: '))
    print(gen_list_pow_odd(n))


if __name__ == "__main__":
    main()
