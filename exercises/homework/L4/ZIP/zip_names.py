# Функция zip_names .
# Принимает 2 аргумента: список с именами и множество с фамилиями.
# Возвращает список с парами значений из каждого аргумента.
# (* Провести эксперименты с различным длинами входных данных.
# Если они одинаковые? А если разные? А если один из них пустой?)
from itertools import zip_longest


def zip_names(lst_names,set_surnames):
    lst = list(zip_longest(lst_names,set_surnames,fillvalue='_'))
    # типизируем объекты листа с tup на list
    for tup in lst:
        tup_ind = lst.index(tup)
        lst[tup_ind] = list(tup)
    return lst



def main():
    names = 'Илья Дмитрий Константин Глеб Данила Конор'
    lst_names = names.split(' ')
    # print(lst_names)
    set_surnames = set('Иванов Короткова Коротков Бобкова Михайлин Максимов Чернигова Василькова'.split(' '))
    #print(set_surnames)
    print(zip_names(lst_names,set_surnames))

if __name__ == '__main__':
    main()