# Функция convert_dict_to_str .
# Принимает 1 аргумент: словарь (в котором значения повторяются).
# Возвращает строку вида “key1=val1 | key2 = val2 | key3 = val3”.
# (* Подсказка: помним про конкатенациюстрок или zip).


def convert_dict_to_str(dict):
    lst = []
    for k, v in zip(dict.keys(),dict.values()):
        lst.append(f'{k} = {v}')
    stre = ' | '.join(lst)
    return stre


def main():
    dict = {'fruit': 'apple', 'vegetable': 'cucumber', 'animal': 'elephant'}
    print(convert_dict_to_str(dict))


if __name__ == "__main__":
    main()
