# Функция convert_list_to_tuple .
# Принимает 2 аргумента: список (с повторяющимися элементами) и значение для поиска.
# Возвращает кортеж (из входного списка) и найдено ли искомое значение(True | False).

def convert_list_to_tuple(lst,value):
    return str(tuple(lst)) + '\n' + f'Искомое значение найдено: {value in lst}'


def main():
    data = 'дом князь 4 8 24 можайник случай случай кукушка 45'
    lst = data.split(' ')
    value_to_search = 'можайникdf'
    print(convert_list_to_tuple(lst,value_to_search))


if __name__ == "__main__":
    main()