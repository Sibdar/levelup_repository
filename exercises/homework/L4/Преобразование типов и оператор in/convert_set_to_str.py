# Функция convert_set_to_str .
# Принимает 2 аргумента: множество и разделитель (строка).
# Возвращает строку полученную разделением элементов множества разделителем,
#           а также количество разделителей в получившейся строке в степени 5.


def convert_list_to_set(some_set,separator):
    return f'Получена следующая строка: {separator.join(sorted(some_set))}'


def main():
    data = 'дом князь 4 8 24 можайник случай случай кукушка 45 45 45 45 45 князь кукушка кукушка'
    separator = ' '
    my_set = set(data.split(separator))
    print(f'Множество к обработке: {my_set}')
    print(convert_list_to_set(my_set,separator))


if __name__ == "__main__":
    main()