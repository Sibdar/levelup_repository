# Функция gen_dict_double.
# Принимает число n.
# Возвращает словарь длиной n, в котором ключ - это значение от 0 до n, а значение - удвоенное значение ключа.
# Пример: n=3, результат {0: 0, 1: 2, 2: 4}.

def gen_dict_double(n):
    return {n:n**2 for n in range(n)}


def main():
    n = int(input('Введите число и я сгенерирую словарь: '))
    print(gen_dict_double(n))


if __name__ == "__main__":
    main()