# Функция gen_dict_double_start_stop . Принимает числа start, stop.
# Возвращает словарь, в котором ключ - это значение от start до stop (не включая), а значение - удвоенное значение ключа.
# Пример: start=3, stop=6, результат {3: 6, 4: 8, 5: 10}.

def gen_dict_double(start, stop):
    return {n:2*n for n in range(start,stop)}


def main():
    stre = input('Введите 2 цифры через пробел: ')
    dig_lst = stre.split(' ')
    for i in dig_lst:
        index_i = dig_lst.index(i)
        dig_lst[index_i] = int(i)
    print(gen_dict_double(dig_lst[0],dig_lst[1]))


if __name__ == "__main__":
    main()