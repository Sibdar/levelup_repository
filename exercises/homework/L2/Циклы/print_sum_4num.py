# Функция print_sum_4num .
# Принимает целое число n.
# Напечатайте четырехзначные числа, сумма цифр которых равна n.
# Если нет ни одного такого, то напечатайте “Not found”.
import random

def print_sum_4num(num):
    a, b, c, d = int(), int(), int(), int()
    for a in range(1,10):
        for b in range(1,10):
            for c in range(1,10):
                for d in range(1,10):
                    if a + b + c + d == num:
                        #print(a * 1000 + b * 100 + c * 10 + d)
                        yield a * 1000 + b * 100 + c * 10 + d



def main():
    print(' | '.join([str(i) for i in list(print_sum_4num(6))]))


if __name__ == '__main__':
    main()