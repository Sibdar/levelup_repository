# Функция print_random_2_between .
# Принимает числа min и max.
# Вывести 3 случайных числа от min до max (включительно) без повторений.


def print_random_2_between(min,max):
    from random import randrange
    return randrange(min,max)


def main():
    min_max = input('Введите 2 числа через пробел: ')
    lst_str = min_max.split(' ')
    lst_int = [int(i) for i in lst_str]
    print(print_random_2_between(lst_int[0], lst_int[1]))


if __name__ == "__main__":
    main()