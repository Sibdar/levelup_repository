# Функция count_user_even_numbers .
# Пользователь вводит ненулевые целые числа до тех пор, пока не введет ноль.
# Верните количество четных чисел, которые он ввел.

def main():
    digt = ''
    count = 0
    while digt != 0:
        digt = int(input('Введите число: '))
        if digt % 2 != 0:
            continue
        else:
            count += 1
    print(count - 1)


if __name__ == '__main__':
    main()