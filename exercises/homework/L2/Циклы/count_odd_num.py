# Функция count_odd_num .
# Принимает натуральное число.
# Верните количество нечетных цифр в этом числе.

def count_odd_num(n):
    count = 0
    for i in str(n):
        if int(i) % 2 != 0:
            count += 1
    return f'Кол-во нечетных цифр: {count}'

def main():
    print(count_odd_num(7777777111112222777779999))


if __name__ == "__main__":
    main()