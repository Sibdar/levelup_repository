# Функция count_num_between .
# Принимает числа n, min и max.
# Верните количество нечетных цифр в этом числе, которые больше min, но меньше max.


def count_num_between(n,min,max):
    count = 0
    for i in str(n):
        if int(i) % 2 != 0:
            count += 1
    if min < count <= max:
        return count
    else:
        return f'Кол-во "{count}" не входит в заданные ограничения "{min} - {max}"'


def main():
    print(count_num_between(87911117179999999999111,20,22))


if __name__ == "__main__":
    main()