# Функция print_hi .
# Принимает число n.
# Выведите на экран n раз фразу "Hi, friend!"

def print_hi(n):
    return n * 'Hi, friend!\n'


def main():
    print(print_hi(9))


if __name__ == "__main__":
    main()