# Функция print_coordinates .
# Принимает координаты x, y.
# Возвращает строку в виде “Цель: 56.879,22.554”.

def print_coords(x, y):
    return f'Цель: {x},{y}'#.format(x=x, y=y)


def main():
    coords = input('Введите координаты "x" и "y" через пробел: ')
    lst_coords = coords.split(' ')
    print(print_coords(lst_coords[0], lst_coords[1]))


if __name__ == "__main__":
    main()
