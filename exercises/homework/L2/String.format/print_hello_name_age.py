# Функция print_hello_name_age .
# Принимает first_name, last_name, age.
# Возвращает строку в виде “Здравствуйте, Иванов Петр. Как хорошо, что вам всего 66!”.

def hello_name_age(firstn,lastn,age):
    return 'Здравствуйте, {ln} {fn}. Как хорошо, что вам всего {age}! Все впереди!'.format(
        ln=lastn, fn=firstn, age=age
    )


def main():
    stre = input('\nВведите имя, фамилию и возраст через запятую (без пробелов): ')
    data = stre.split(',')
    print('\n', hello_name_age(data[1], data[0], data[2]))


if __name__ == '__main__':
    main()
