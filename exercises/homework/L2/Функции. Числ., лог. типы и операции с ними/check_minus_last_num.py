# Функция check_minus_last_num .
# Принимает трехзначное число.
# Вернуть результат вычисления выражения: число - (последняя цифра числа).

def check_minus_last_num(num):
    return num - int(str(num)[-1])


def main():
    print(check_minus_last_num(758))


if __name__ == "__main__":
    main()