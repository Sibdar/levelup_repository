# Функция max_number .
# Принимает 3 числа.
# Верните наибольшее число.

def max_number(n1, n2, n3):
    return max(n1, n2, n3)


def main():
    print(max_number(54, 114, 45))


if __name__ == "__main__":
    main()