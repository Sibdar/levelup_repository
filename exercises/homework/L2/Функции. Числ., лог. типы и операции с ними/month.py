#Функция month . Принимает номер месяца. Возвращает название месяца, если месяца с таким
#номером не существует, то возвращает строку “Error”.


def month(num):
    months = {1:"January", 2:"February",3:"March",4:"April",5:"May",6:"June",
          7:"July",8:"August",9:"September",10:"October",11:"November",12:"December"}
    return months[num]

def main():
    while True:
        n = int(input("\nВведите номер месяца: "))
        if 1 <= n <= 12:
            print("\nОтвет :", month(n))
        elif n == 0:
            break
        else:
            print(f"\nМесяца '{n}' не существует. Введите данные типа 'int' от 1 до 12")


if __name__ == '__main__':
    main()


