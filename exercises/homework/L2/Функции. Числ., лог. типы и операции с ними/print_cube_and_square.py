# Функция print_cube_and_square . Принимает число. Выведите на экран квадрат этого числа, куб этого
# числа.
def cube_square(num):
    return 'Квадрат числа "{num}": {num_sq}'.format(num=num, num_sq=num**2) + '\n' \
           'Куб числа "{num}": {num_cube}'.format(num=num, num_cube=num**3)


print(cube_square(231561321456))
