# Функция check_num_palindrome .
# Принимает четырехзначное число.
# Если оно читается слева направо и справа налево одинаково, то вернуть True, иначе False.


def check_num_palindrome(n):
    n_str = str(n)
    return n_str == n_str[::-1]
    # if n_str == n_str[::-1]:
    #     return True
    # else:
    #     return False


def main():
    print(check_num_palindrome(1241))


if __name__ == "__main__":
    main()