# Функция celsius_to_fahrenheit .
# Принимает значение температуры в градусах Цельсия.
# Вернуть температуру в градусах Фаренгейта.

def celsius_to_fahrenheit(t_cel):
    return f'Результат: {t_cel * 1.8 + 32} градусов Фаренгейта'


def main():
    inp = float(input('Введите граудсы Цельсия: '))
    print(celsius_to_fahrenheit(inp))


if __name__ == "__main__":
    main()