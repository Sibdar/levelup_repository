# Функция sub_numbers_in_str . Принимает строку. Возвращает сумму имеющихся в ней цифр.

def calc(stre):
    sum = 0
    for i in stre:
        if i.isdigit():
            sum += int(i)
    return sum


def main():
    while True:
        stre = input("\nВведите строку и я верну сумму имеющихся в ней цифр: ")
        print('Сумма цифр в строке: ', calc(stre))


if __name__ == '__main__':
    main()
