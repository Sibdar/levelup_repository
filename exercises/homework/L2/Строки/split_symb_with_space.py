# Функция split_symb_with_space .
# Принимает строку.
# Вставить после каждого символа пробел.
# Вернуть получившуюся строку.

def split_symb_with_space(string):
    return string[0] + string[1:].replace('','_')


def main():
    while True:
        stre = input('Введите строку: ')
        # stre = 'куканишвили'
        print(stre)
        print(split_symb_with_space(stre))

if __name__ == "__main__":
    main()