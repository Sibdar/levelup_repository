# Функция count_numbers . Принимает строку. Возвращает количество цифр в данной строке.

def count_number(string):
    digit = 0
    for i in string:
        if i.isdigit():
            digit += 1
    return digit


def main():
    while True:
        stre = input("\nВведите данные и я посчитаю кол-во цифр: ")
        if stre == "exit":
            break
        else:
            print("\nВ данном тексте выявлено цифр: ", count_number(stre))

if __name__ == '__main__':
    main()
