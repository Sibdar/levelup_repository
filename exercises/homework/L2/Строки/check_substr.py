# Функция check_substr .
# Принимает две строки.
# Если меньшая по длине строка содержится в большей,то возвращает True, иначе False.

def check_substr(str1,str2):
    return str1 in str2 or str2 in str1


def main():
    stre = input('Введите 2 строки через пробел: ')
    lst = stre.split(' ')
    print(check_substr(lst[0],lst[1]))


if __name__ == "__main__":
    main()