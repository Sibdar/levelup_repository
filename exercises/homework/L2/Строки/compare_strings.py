# Функция compare_strings .
# Принимает две строки. Сравнивает эти две строки.
# Возвращает True при равенстве, иначе False.

def compare_strings(str1,str2):
    if str1 == str2:
        return f'Строки "{str1}" и "{str2}" идентичны.'
    else:
        return f'Строки "{str1}" и "{str2}" не идентичны.'


def main():
    while True:
        strings = input('\nВведите 2 строки(через пробел) и я их сравню: ')
        lst_strings = strings.split(' ')
        print(compare_strings(lst_strings[0],lst_strings[1]))


if __name__ == "__main__":
    main()