# Функция remove_question . Удалите в строке все символы "?". Верните результат.

def remove_question(stre):
    if '?' in stre:
        return stre.replace('?','')
    else:
        return 'изменения не осуществлены, символ "?" не обнаружен.'


def main():
    inp_stre = input("Введите данные: ")
    print('Данные обработаны: ', remove_question(inp_stre))


if __name__ == '__main__':
    main()
