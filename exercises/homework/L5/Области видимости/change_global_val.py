# В глобальной области видимости определена переменная my_var с каким-то значением.
# В функции main выводим значение my_var на экран.
# В функции change_global_val меняем значение глобальной переменной my_var на другое.
# После этого в функции main опять выводим значение my_var на экран.


def change_global_val():
    global my_var
    my_var = 'Синий'


my_var = 'Красный'
def main():
    print(my_var)
    change_global_val()
    print(my_var)


if __name__ == '__main__':
    main()
