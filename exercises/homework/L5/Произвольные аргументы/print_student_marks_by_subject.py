# Определить функцию print_student_marks_by_subject .
# Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам ( **kwargs).
# Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).
# Пример вызова функции print_student_marks_by_subject(“Вася”, math=5, biology=(3, 4), magic=(4, 5, 5)).

def print_student_marks_by_subject(st_name,**kwargs):
    # избавляемся от тьюплов в значениях словаря (ибо раздражают) и типизируем в строки
    for i in kwargs:
        if type(kwargs[i]) == tuple:
            kwargs[i] = ','.join([str(x) for x in kwargs[i]])
    stre = ', '.join([f'{x}:{y}' for x,y in kwargs.items()])
    return f'Name: {st_name} ; Subjects: {stre}'


def main():
    print(print_student_marks_by_subject('Вася',math=5,physics=(4,3,5,4), it=(4,5,5)))


if __name__ == "__main__":
    main()