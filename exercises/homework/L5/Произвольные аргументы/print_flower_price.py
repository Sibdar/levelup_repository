# Определить функцию print_flower_price .
# Принимает 2 аргумента: название цветка (строка) и неопределенное количество цен (*args).
# Функция print_flower_price выводит вначале название цветка, а потом все цены на него.
# В функции main вызывается функция print_flower_price и передается название цветка и произвольное количество цен.
# Например, print_flower_price(‘rose’, 77, 10, 50, 99) или print_flower_price(‘tulp’, 100, 44, 777, 876, 555, 111).


def print_flower_price(flower_name,*prices):
    prices_str = ', '.join([str(price) + ' RUB' for price in prices])
    #prices_final = prices_str.replace(',','$,')
    return f'{flower_name}: {prices_str}'


def main():
   print(print_flower_price('rose', 100, 44, 777, 876, 555, 111))


if __name__ == '__main__':
    main()