# lambda делит аргумент a на аргумент b и выводит результат.
print('Результат деления "a" и "b": {lambda_div}'.format(lambda_div = int((lambda a, b: a / b)(200,10))))

# lambda суммирует аргументы a, b и c и выводит результат.
print('Результат суммирования "a","b" и "c": {lambda_sum}'.format(lambda_sum = (lambda a,b,c: a + b + c)(45,25,864)))

# lambda возводит в квадрат переданный аргумент, возвращает результат
print('Результат a**2: {0}'.format((lambda a: a**2)(54)))

# lambda перемножает аргументы a,b и с и выводит результат.
input_str = input('Введите "a","b" и "c" через пробел и я их переумножу: ')
lst_stre = input_str.split(' ')
lst_dgts = [int(stre) for stre in lst_stre]
print('Результат перемножения "a","b" и "c": {lambda_mult}'
      .format(lambda_mult = (lambda a,b,c: a * b * c)(lst_dgts[0], lst_dgts[1], lst_dgts[2])))

