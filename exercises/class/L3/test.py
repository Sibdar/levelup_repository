names = ["Il","Bob","Kate"]

# длина
len(names)

# удаление элемента списка
del names[0]

# удаление с шагом 2 (каждого второго)
del names[::2]

# добавление элемента списка
names[3] = 'Thorn'

